'use strict'

var fs = require('fs')
var path = require('path')
var chalk = require('chalk')
var duplicated = require('array-duplicated')
var yaml = require('js-yaml')
var not = require('not')
var hidden = require('is-hidden')
var pkg = require('../package.json')
const {loadPattern} = require('../lib/utils')

var join = path.join
var extname = path.extname

var root = 'data'

// Generate all languages.
fs.readdirSync(root)
  .filter(not(hidden))
  .map(function (language) {
    return {language: language, root: join(root, language)}
  })
  .forEach(generateLanguage)

function generateLanguage(info) {
  var patterns = fs
    .readdirSync(info.root)
    .filter(not(hidden))
    .filter(function (basename) {
      return extname(basename) === '.yml'
    })
    .map(function (basename) {
      return yaml.load(String(fs.readFileSync(join(info.root, basename))))
    })
    .reduce(function (all, cur) {
      return all.concat(cur)
    }, [])

  var data = patterns.map(loadPattern)

  // Check patterns.
  var phrases = []

  data.forEach(function (entry) {
    if (entry.type !== 'basic' && entry.categories.length < 2) {
      throw new Error(
        'Use `type: basic` for single entries with one category: ' +
          Object.keys(entry.inconsiderate).join(', ')
      )
    }

    if (entry.inconsiderate) {
      Object.keys(entry.inconsiderate).forEach(function (inconsiderate) {
        phrases.push(inconsiderate)

        if (/-/.test(inconsiderate)) {
          throw new Error(
            'Refrain from using dashes inside inconsiderate terms: they’ll be stripped when looking for words: ' +
              Object.keys(entry.inconsiderate).join(', ')
          )
        }

        if (/['’]/.test(inconsiderate) && !entry.apostrophe) {
          throw new Error(
            'Refrain from using apostrophes inside inconsiderate terms, they’ll be stripped when looking for words (or use `apostrophe: true`): ' +
              Object.keys(entry.inconsiderate).join(', ')
          )
        }
      })
    }
  })

  // Check for duplicates.
  var duplicates = duplicated(phrases)

  if (duplicates.length !== 0) {
    throw new Error(
      'Refrain from multiple entries:\n  ' + duplicates.join(', ')
    )
  }

  var basename = info.language + '.json'
  var scriptname = info.language + '.js'

  // Write patterns.
  fs.writeFileSync(join('lib', basename), JSON.stringify(data, null, 2) + '\n')

  console.log(chalk.green('✓') + ' wrote `lib/' + basename + '`')

  fs.writeFileSync(
    scriptname,
    [
      "'use strict'",
      '',
      "var factory = require('./lib/factory')",
      "var patterns = require('./lib/" + basename + "')",
      '',
      "module.exports = factory(patterns, '" + info.language + "')",
      ''
    ].join('\n')
  )

  console.log(chalk.green('✓') + ' wrote `' + scriptname + '`')

  if (pkg.files.indexOf(scriptname) === -1) {
    throw new Error(
      'Please add `' + scriptname + '` to `files` in `package.json`'
    )
  }
}
