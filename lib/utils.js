var unique = require('array-unique')

// Clean a value.
function clean(value) {
  var copy

  if (typeof value === 'string') {
    value = [value]
  }

  if (value && 'length' in value && value.length !== 0) {
    copy = value
    value = {}

    copy.forEach(function (phrase) {
      value[phrase] = 'a' // Example category
    })
  }

  return value
}

exports.loadPattern = (entry, { idSuffix } = {}) => {
  var note = entry.note
  var source = entry.source
  var inconsiderate = clean(entry.inconsiderate)
  var categories = {}
  var parts = []
  var phrase
  var category

  if (source) {
    if (note) {
      note += ' (source: ' + source + ')'
    } else {
      note = 'Source: ' + source
    }
  }

  for (phrase in inconsiderate) {
    category = inconsiderate[phrase]

    if (!categories[category] || categories[category].length > phrase.length) {
      categories[category] = phrase
    }
  }

  for (phrase in categories) {
    parts.push(categories[phrase].replace(/[\s.]+/g, '-'))
  }

  return {
    id: `${parts.sort().join('-').toLowerCase()}-${idSuffix}`,
    type: entry.type,
    apostrophe: entry.apostrophe ? true : undefined,
    categories: unique(Object.values(inconsiderate)),
    considerate: clean(entry.considerate),
    inconsiderate: inconsiderate,
    condition: entry.condition,
    note: note,
    source,
    response: entry.response
  }
}
