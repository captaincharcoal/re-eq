'use strict'

var factory = require('./lib/factory')
var patterns = require('./lib/en.json')

module.exports = (extendedPatterns = []) => {
  return factory([...extendedPatterns, ...patterns], 'en')
}
